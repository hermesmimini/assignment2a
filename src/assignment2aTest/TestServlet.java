package assignment2aTest;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TestServlet() {
        super();
    }
    
    @Override
    public void init(ServletConfig config)throws ServletException {
    	System.out.println("=======> Test Servlet init(");
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("=======> Test Servlet doGet(");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		request.setAttribute("firstName", firstName);
		request.setAttribute("lastName", lastName);
		request.getRequestDispatcher("TestResponse.jsp").forward(request, response);
		System.out.println(firstName);
		System.out.println(lastName);	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("=======> Test Servlet doPost(");
		doGet(request, response);
	}
	
	@Override
	public void destroy() {
		System.out.println("=======> Test Servlet destroy(");
	}
	
	
	

}
